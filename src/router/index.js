import Vue from 'vue'
import VueRouter from 'vue-router'
import CatalogPage from '@/views/CatalogPage'
import DeliveryPage from '@/views/DeliveryPage'
import PaymentPage from '@/views/PaymentPage'
import ContactsPage from '@/views/ContactsPage'
import AboutPage from '@/views/AboutPage'



Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'catalog',
    component: CatalogPage,
    meta: {
      title: 'Картины эпохи Возрождения'
    }
  },
  {
    path: '/delivery',
    name: 'delivery',
    component: DeliveryPage,
    meta: {
      title: 'Доставка'
    }
  },
  {
    path: '/payment',
    name: 'payment',
    component: PaymentPage,
    meta: {
      title: 'Оплата'
    }
  },
  {
    path: '/contacts',
    name: 'contacts',
    component: ContactsPage,
    meta: {
      title: 'Контакты'
    }
  },
  {
    path: '/about',
    name: 'about',
    component: AboutPage,
    meta: {
      title: 'О галерее'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
